﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Strongchest")>
<Assembly: AssemblyDescription("Protect your files with strong encryption")>
<Assembly: AssemblyCompany("Giacomo Berti")>
<Assembly: AssemblyProduct("Strongchest")>
<Assembly: AssemblyCopyright("Giacomo Berti")>
<Assembly: AssemblyTrademark("Giacomo Berti")>

<Assembly: ComVisible(True)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("7b764125-b6a9-45bf-8522-b5c1a05ea704")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("2.0.2.0")>
<Assembly: AssemblyFileVersion("2.0.2.0")>
