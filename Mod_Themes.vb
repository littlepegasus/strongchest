﻿Module Mod_Themes

    'Tema scuro
    Public darkPopUp = Color.FromArgb(39, 41, 45)
    Public darkNavBar = Color.FromArgb(39, 41, 45)
    Public darkPrimary = Color.FromArgb(30, 31, 32)
    Public darkSecondary = Color.FromArgb(32, 33, 34)
    Public darkButton = Color.FromArgb(41, 42, 43)
    Public darkSecondaryButton = Color.FromArgb(59, 61, 64)
    Public darkTextBox = Color.FromArgb(59, 61, 64)
    Public darkProgressBar = Color.FromArgb(40, 41, 42)
    Public darkSecondaryNavBar = Color.FromArgb(58, 64, 69)
    Public darkPrimaryText = Color.Gainsboro
    Public darkSecondaryText = Color.Gray

    'Tema chiaro
    Public lightPopUp = Color.WhiteSmoke
    Public lightNavBar = Color.DarkGray
    Public lightPrimary = Color.Gainsboro
    Public lightSecondary = Color.WhiteSmoke
    Public lightButton = Color.Linen
    Public lightProgressBar = Color.Linen
    Public lightSecondaryNavBar = Color.Gray
    Public lightSecondaryButton = Color.FromArgb(231, 239, 246)
    Public lightTextBox = Color.FromArgb(231, 239, 246)
    Public lightPrimaryText = Color.FromArgb(23, 24, 25)
    Public lightSecondaryText = Color.FromArgb(41, 42, 43)


End Module
